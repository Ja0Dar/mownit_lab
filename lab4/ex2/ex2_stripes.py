from ex2 import BasicImg
from typing import Tuple
import numpy as np


# import numba


class StripeBasicImg(BasicImg):
    def _points_with_neighbour(self, neighbour: Tuple[int, int]):
        return self._neighbours_of(neighbour)

    def _point_energy(self, point: Tuple[int, int]):
        e = 0
        i, j = point
        color = self.state[i, j]
        A = self.state
        for ni, nj in self._neighbours_of(point):
            if (nj == j and A[ni, nj] != color) or (nj != j and A[ni, nj] == color):
                e += 1
        return e

    def _neighbours_of(self, point: Tuple[int, int]):
        i, j = point
        # return ((x % self.n, y % self.n)
        #         for x in range(i - 1, i + 2)
        #         for y in range(j - 1, j + 2)
        #         if x != i or y != j)
        im1 = (i - 1) % self.n
        ip1 = (i + 1) % self.n
        jm1 = (j - 1) % self.n
        jp1 = (j + 1) % self.n
        return [(im1, jm1),
                (im1, j),
                (im1, jp1),
                (i, jm1),
                (i, jp1),
                (ip1, jm1),
                (ip1, j),
                (ip1, jp1)]


def main():
    from generate import generate_image_matrix
    from image_io import save_results,load_results, plot_image_results

    A = generate_image_matrix(128, 0.3)
    prob = StripeBasicImg(A, pixels_to_change=1)
    import simulated_annealing as sa

    # 1e5 iterations is reasonable : few dozen seconds
    # res, e_arr = sa.simulated_annealing(sa.square_dT(15), prob, max_i=1e5, E_interval=1)
    # res, e_arr = sa.simulated_annealing(sa.linear_dT(15), prob, max_i=1e5,E_interval=1)
    # res, e_arr = sa.simulated_annealing(sa.default_dT(15, 1), prob, max_i=1e5, E_interval=1)

    # save_results("strip",A,res,e_arr)

    A,res,e_arr= load_results("strip/strip")
    plot_image_results(A, res, e_arr)


if __name__ == '__main__':
    main()
