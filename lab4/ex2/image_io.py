import numpy as np
import matplotlib.pyplot as plt


def plot_image(A, show=True):
    plt.imshow(A)
    if show:
        plt.show()


def plot_image_results(start_state, end_state, energy_array, auto_show=True, title=None):
    fig, ax = plt.subplots(3)
    ax[0].imshow(start_state)
    ax[1].imshow(end_state)

    ax[2].plot(energy_array[:, 0], energy_array[:, 1])
    ax[2].autoscale(tight=False)

    if title is not None:
        plt.title(title)

    if auto_show:
        plt.show()


def save_results(name, start_state, final_state, E, directory="saved"):
    np.save("{}/{}_start".format(directory, name), start_state)
    np.save("{}/{}_final".format(directory, name), final_state)
    np.save("{}/{}_E".format(directory, name), E)


def load_results(name, directory="saved"):
    start_state = np.load("{}/{}_start.npy".format(directory, name))
    finish_state = np.load("{}/{}_final.npy".format(directory, name))
    E = np.load("{}/{}_E.npy".format(directory, name))
    return start_state, finish_state, E
