import numpy as np
from ex2 import Color


def generate_image_matrix(n: int, ro: float) -> np.ndarray:
    A = np.random.uniform(size=(n * n, 1))
    A = np.fromiter((Color.black.value if x < ro else Color.white.value for x in A), A.dtype)
    return np.reshape(A, (n, n))


if __name__ == '__main__':
    a = generate_image_matrix(4, 0.3)
    import plot_image as pi

    pi.plot_image(a)
