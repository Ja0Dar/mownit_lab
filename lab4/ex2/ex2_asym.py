from ex2 import BasicImg, Color
from typing import Tuple
import simulated_annealing as sa, numpy as np


class AsymNeighImg(BasicImg):
    def _points_with_neighbour(self, neighbour: Tuple[int, int]):
        i, j = neighbour
        im2 = (i - 2) % self.n
        im1 = (i - 1) % self.n
        jm2 = (j - 2) % self.n
        jm1 = (j - 1) % self.n
        return ((im2, jm2),
                (im2, jm1),
                (im2, j),
                (im1, jm2),
                (im1, jm1),
                (im1, j),
                (i, jm2),
                (i, jm1))
        # return ((row % self.n, col % self.n)
        #         for row in range(i - 2, i + 1)
        #         for col in range(j - 2, j + 1)
        #         if row != i or col != j)

    def _neighbours_of(self, point: Tuple[int, int]):
        i, j = point

        ip1 = (i + 1) % self.n
        ip2 = (i + 2) % self.n
        jp1 = (j + 1) % self.n
        jp2 = (j + 2) % self.n
        return ((ip2, jp2),
                (ip2, jp1),
                (ip2, j),
                (ip1, jp2),
                (ip1, jp1),
                (ip1, j),
                (i, jp2),
                (i, jp1))
        # return ((row % self.n, col % self.n)
        #         for row in range(i, i + 3)
        #         for col in range(j, j + 3)
        #         if row != i or col != j)

    def _point_energy(self, point: Tuple[int, int]):
        i, j = point
        A = self.state
        color = A[i, j]
        if color == Color.white.value:
            return 0
        # only for black pixels:
        e = 0
        for ni, nj in self._neighbours_of(point):
            if (ni == i + 2 or nj == j + 2) and not (ni == i + 2 and nj == j + 2):
                # push away
                if A[ni, nj] == color:
                    e += 1
            else:
                if A[ni, nj] != color:
                    e += 1
        return e


def main():
    from generate import generate_image_matrix
    from image_io import plot_image_results, save_results, load_results

    A = generate_image_matrix(513, 0.3)
    prob = AsymNeighImg(A, pixels_to_change=3)

    # 1e5 iterations is reasonable : few dozen seconds
    # res, e_arr = sa.simulated_annealing(sa.default_dT(np.float64(30), np.float64(0.01)), prob, max_i=1.5e7,
    #                                     E_interval=20)
    # plot_image_results(A, res, e_arr)


    # save_results("asym",A,res,e_arr)
    X = load_results("asym", directory="saved/asym")
    plot_image_results(*X)


if __name__ == '__main__':
    main()
