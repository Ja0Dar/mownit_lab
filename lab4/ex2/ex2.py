import simulated_annealing as sa
import numpy as np
from copy import deepcopy
from enum import Enum
from typing import Tuple, Iterable


class Color(Enum):
    white = 0
    black = 1


class IllegalStateError(Exception):
    pass


class BasicImg(sa.Problem):
    def __init__(self, state, pixels_to_change=5) -> None:
        super().__init__(deepcopy(state))
        self.pixels_to_change = pixels_to_change
        self.n = state[:, 0].size
        self.destination_cords = list()
        self.source_cords = list()

        self._prev_energy = None
        self._energy = self._calculate_energy()

    def move(self):

        self.source_cords = self._get_random_points(Color.black)
        self.destination_cords = self._get_random_points(Color.white)

        self._swap_state()

        self._prev_energy = self._energy
        self._energy = self._prev_energy + self._energy_delta()

    def move_undo(self):
        if self._prev_energy is None:
            raise IllegalStateError("cannot undo when no move was performed")
        self._swap_state()
        self._energy, self._prev_energy = self._prev_energy, self._energy

    def _get_random_points(self, color: Color):
        x = np.random.randint(0, self.n - 1)
        y = np.random.randint(0, self.n - 1)
        res = []
        for i in range(self.pixels_to_change):
            while self.state[x, y] != color.value or (x, y) in res:
                x = np.random.randint(0, self.n - 1)
                y = np.random.randint(0, self.n - 1)
            res.append((x, y))
        return res

    def energy(self):
        return self._energy

    def _swap_state(self):
        for i in range(self.pixels_to_change):
            sx, sy = self.source_cords[i]
            dx, dy = self.destination_cords[i]
            self.state[[sx, dx], [sy, dy]] = self.state[[dx, sx], [dy, sy]]

    def _calculate_energy(self):
        e = 0
        for i in range(self.n):
            for j in range(self.n):
                e += self._point_energy((i, j))
        return e

    def _energy_delta(self):
        changed_points = self.source_cords + self.destination_cords
        pts_with_changed_e = set(changed_points)
        for i, j in changed_points:
            pts_with_changed_e.update(self._points_with_neighbour((i, j)))

        new_area_energy = self._area_energy(pts_with_changed_e)
        self._swap_state()

        old_area_energy = self._area_energy(pts_with_changed_e)
        self._swap_state()

        return new_area_energy - old_area_energy

    def _area_energy(self, points_to_consider: Iterable[Tuple[int, int]]):
        return sum(map(
            lambda tup: self._point_energy(tup),
            points_to_consider))

    def _point_energy(self, point: Tuple[int, int]):
        i, j = point
        color = self.state[i, j]
        e = 0
        for ni, nj in self._neighbours_of(point):
            if self.state[ni, nj] != color:
                e += 1
        return e

    def _neighbours_of(self, point: Tuple[int, int]):
        # points that are considered in calculating point energy
        i, j = point
        return ((i - 1) % self.n, j), (i, (j - 1) % self.n), ((i + 1) % self.n, j), (i, (j + 1) % self.n)

    def _points_with_neighbour(self, neighbour: Tuple[int, int]):
        # points that consider "neighbour"  in calculating their energy

        # symmetric neighbourhood, therefore:
        return self._neighbours_of(neighbour)


def main():
    from generate import generate_image_matrix
    from image_io import save_results, load_results, plot_image_results

    A = generate_image_matrix(120, 0.3)
    prob = BasicImg(A, pixels_to_change=1)
    import simulated_annealing as sa

    res, e_arr = sa.simulated_annealing(sa.default_dT(700, 0.001), prob, max_i=1e7,E_interval=8)
    # save_results("4nghb", A, res, e_arr)
    # A, res, e_arr = load_results("4nghb_700/4nghb")
    plot_image_results(A, res, e_arr)


if __name__ == '__main__':
    main()
