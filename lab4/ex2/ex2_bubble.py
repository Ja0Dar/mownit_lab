from ex2 import BasicImg, Color
from typing import Tuple
import numpy as np

class BubbleImg(BasicImg):
    def _points_with_neighbour(self, neighbour: Tuple[int, int]):
        return self._neighbours_of(neighbour)

    def _point_energy(self, point: Tuple[int, int]):
        e = 0
        i, j = point
        A = self.state
        color = A[i, j]
        if color == Color.white.value:
            return 0

        for ni, nj in self._neighbours_of(point):
            if max(abs(ni - i), abs(nj - j)) < 2:
                # closer
                if A[ni, nj] != color:
                    e += 1
            else:
                if A[ni, nj] == color:
                    e += 1
        return e

    def _neighbours_of(self, point: Tuple[int, int]):
        i, j = point
        # return ((x % self.n, y % self.n)
        #         for x in range(i - 2, i + 3)
        #         for y in range(j - 2, j + 3)
        #         if x != i or y != j)
        #


        # 1/3 SPEED IMPROVEMENT:
        im2 = (i - 2) % self.n
        im1 = (i - 1) % self.n
        jm2 = (j - 2) % self.n
        jm1 = (j - 1) % self.n
        ip1 = (i + 1) % self.n
        ip2 = (i + 2) % self.n
        jp1 = (j + 1) % self.n
        jp2 = (j + 2) % self.n

        return ((i, jm1),
                (i, jm2),
                (i, jp1),
                (i, jp2),
                (im1, j),
                (im1, jm1),
                (im1, jm2),
                (im1, jp1),
                (im1, jp2),
                (im2, j),
                (im2, jm1),
                (im2, jm2),
                (im2, jp1),
                (im2, jp2),
                (ip1, j),
                (ip1, jm1),
                (ip1, jm2),
                (ip1, jp1),
                (ip1, jp2),
                (ip2, j),
                (ip2, jm1),
                (ip2, jm2),
                (ip2, jp1),
                (ip2, jp2))


def main():
    from generate import generate_image_matrix
    from image_io import plot_image_results, save_results, load_results

    A = generate_image_matrix(512, 0.3)
    prob = BubbleImg(A, pixels_to_change=1)
    import simulated_annealing as sa

    # res, e_arr = sa.simulated_annealing(sa.default_dT(85, 1), prob, max_i=1e7, E_interval=30)
    # save_results("bubble",A,res,e_arr)
    # plot_image_results(A, res, e_arr)

    X = load_results("bubble",directory ="saved/bubble")
    plot_image_results(*X)

if __name__ == '__main__':
    main()

    #neighbourhood computing  optimalization  ;)
    # for i in ["i", "im1", "im2", "ip1", "ip2"]:
    #     for j in ["j", "jm1", "jm2", "jp1", "jp2"]:
    #         if not (i == "i" and j == "j"):
    #             print("({},{}),".format(i, j))
