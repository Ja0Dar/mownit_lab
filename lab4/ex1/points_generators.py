import numpy as np


def random_uniform(n):
    return np.random.uniform(0, 10, (n, 2))


def random_points_normal(n, mu_tuple, sigma_tuple):
    mu_x, mu_y = mu_tuple
    sigma_x, sigma_y = sigma_tuple
    res = np.zeros((n, 2))
    res[:, 0] = np.random.normal(mu_x, sigma_x, n)
    res[:, 1] = np.random.normal(mu_y, sigma_y, n)
    return np.abs(res)


def random_points_clusters(n, cluster_no):
    centres = np.random.uniform(0, 10, (cluster_no, 2))
    sigma = 0.15

    c_size = n // cluster_no

    points = np.zeros((n, 2))
    c_x, c_y = None, None
    for i, centre in enumerate(centres):
        c_x, c_y = centre
        points[i * c_size:(i + 1) * c_size, 0] = np.random.normal(c_x, sigma, (c_size))
        points[i * c_size:(i + 1) * c_size, 1] = np.random.normal(c_y, sigma, (c_size))

    j = len(centres) - 1
    points[j * c_size:n, 0] = np.random.normal(c_x, sigma, (n - j * c_size))
    points[j * c_size:n, 1] = np.random.normal(c_y, sigma, (n - j * c_size))

    return points


if __name__ == '__main__':
    p = random_points_clusters(54, 10)
    import matplotlib.pyplot as plt

    plt.scatter(p[:, 0], p[:, 1])
    plt.show()
