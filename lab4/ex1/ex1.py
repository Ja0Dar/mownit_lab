from  copy import deepcopy
from random import randint

import numpy as np
import points_generators as pg

import simulated_annealing as sa
from plot_tsp import *


class TSP(sa.Problem):
    def __init__(self, state, consecutive_swap=False) -> None:
        super().__init__(deepcopy(state))
        self.consecutive_swap = consecutive_swap
        self.i1 = None
        self.i2 = None
        self.n = state[:, 0].size

    def move(self):
        self.i1 = randint(0, self.n - 1)
        self.i2 = randint(0, self.n - 1)
        self.swap_state()

    def move_undo(self):
        self.swap_state()

    def energy(self):
        E = 0
        x1, y1 = self.state[0]
        for i in range(1, self.state[:, 0].size):
            x2, y2 = self.state[i]

            dx = x1 - x2
            dy = y1 - y2

            E += (dx * dx + dy * dy)

            x1, y1 = x2, y2

        return E

    def prev_energy(self):
        raise NotImplementedError("prev ex")

    def swap_state(self):
        if self.consecutive_swap:
            self.state[[self.i1, (self.i1 + 1) % self.n], :] = self.state[[(self.i1 + 1) % self.n, self.i1], :]
        else:
            self.state[[self.i1, self.i2], :] = self.state[[self.i2, self.i1], :]


def ex1_1():
    def uniform():
        # uniform:

        args= [(20, 50, 0.1), (40, 300, 1), (60, 163, 2.5)]
        for n, Tmax, Tmin in args:
            A = pg.random_uniform(n)
            tsp = TSP(A)
            opt_state, e_array = sa.simulated_annealing(sa.default_dT(Tmax, Tmin), tsp,
                                                        max_i=int(1e4))
            plot_TSP_results(opt_state, e_array, auto_show=False, title="uniform, {} points".format(n))

    def normal(wide=False,):
        # normal wide stripe
        A = pg.random_points_normal(50, (1, 1), (0.1, 1))
        tsp = TSP(A)
        opt_state, e_arr = sa.simulated_annealing(sa.default_dT(4, 0.01), tsp, max_i=1e5, E_interval=1)
        plot_TSP_results(opt_state, e_arr, auto_show=False, title="normal 50, horizontal stripe")

        A = pg.random_points_normal(20, (10, 10), (1, 1))
        np.random.shuffle(A)
        tsp = TSP(A)
        opt_state, e_arr = sa.simulated_annealing(sa.default_dT(10, 0.14), tsp, max_i=1e4, E_interval=2)
        plot_TSP_results(opt_state, e_arr, auto_show=False, title="normal 20")

        A = pg.random_points_normal(35, (10, 10), (0.5, 1))
        np.random.shuffle(A)
        tsp = TSP(A)
        opt_state, e_arr = sa.simulated_annealing(sa.default_dT(10, 0.01), tsp, max_i=1e4, E_interval=2)
        plot_TSP_results(opt_state, e_arr, title="normal 35")

    def clusters():
        c = 9
        for n, temp, max_i in [(100, 60, 1.5e5), (80, 60, 5e4), (50, 60, 5e4)]:
            A = pg.random_points_clusters(n, c)
            np.random.shuffle(A)
            tsp = TSP(A)
            opt_state, e_arr = sa.simulated_annealing(sa.default_dT(temp, 1.7), tsp, max_i=max_i)
            plot_TSP_results(opt_state, e_arr, auto_show=False, title="{} points, {} clusters".format(n, c))


    # uniform()
    # normal()
    clusters()
    plt.show()
    pass


def ex1_2():
    n = 80
    Tmax=70
    Tmin=2
    A = pg.random_uniform(n)
    tsp = TSP(A)
    opt_state, e_array = sa.simulated_annealing(sa.default_dT(Tmax, Tmin), tsp, max_i=1e5)
    print("arbitrary : Emin={}".format(np.min(e_array[:, 1])))
    plot_TSP_results(opt_state, e_array, auto_show=False, title="uniform, {} points".format(n))

    tsp = TSP(A, True)
    opt_state, e_array = sa.simulated_annealing(sa.default_dT(Tmax,Tmin), tsp, max_i=1e5)
    print("consecutive: Emin={}".format(np.min(e_array[:, 1])))
    plot_TSP_results(opt_state, e_array, title="uniform, {} points consecutive swap".format(n))

    # arbitrary works better every time


if __name__ == '__main__':
    ex1_1()  # uncoment specific example in ex_1
    # ex1_2()