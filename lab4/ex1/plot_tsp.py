import networkx as nx
import matplotlib.pyplot as plt


def plot_TSP(A, ax=None):
    G = nx.Graph()
    x, y = A[0]
    pos = {0: (x, y)}
    for i in range(1, A[:, 0].size):
        G.add_edge(i - 1, i)
        x, y = A[i]
        pos[i] = (x, y)

    # nx.draw(G, pos=pos, node_size=10, ax=ax)
    nx.draw_networkx(G, pos=pos, ax=ax, node_size=10, with_labels=False)


def scatter_plot(data):
    f, ax = plt.subplots(1)
    ax.scatter(data[:, 0], data[:, 1])
    ax.autoscale(tight=False)

    ax.set_ylim(ymin=0)
    ax.set_xlim(xmin=0)

    plt.show(f)


def plot_TSP_results(end_state, energy_array, auto_show=True, title=None):
    fig, ax = plt.subplots(2)
    plot_TSP(end_state, ax[0])

    ax[1].plot(energy_array[:, 0], energy_array[:, 1])
    ax[1].autoscale(tight=False)

    if title is not None:
        plt.title(title)

    if auto_show:
        plt.show()
