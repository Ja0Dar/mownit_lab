from unittest import TestCase
from ex2.ex2 import BasicImg
from ex2.generate import generate_image_matrix
from copy import deepcopy
import numpy as np

class TestPicture(TestCase):
    def setUp(self):
        A = generate_image_matrix(40, 0.3)
        self.prob = BasicImg(A)

    def test_energy_delta(self):
        e = self.prob._calculate_energy()

        for i in range(200):
            self.prob.move()
            e += self.prob._energy_delta()
            self.assertEqual(e, self.prob._calculate_energy())

    def test_energy(self):
        prob = self.prob
        prev_e = prob.energy()
        for i in range(300):
            prob.move()
            prob.move_undo()
            self.assertEqual(prev_e, prob.energy())

    def test_move_undo(self):
        prob = self.prob
        prev = deepcopy(prob.state)
        for i in range(300):
            prob.move()
            state = deepcopy(prob.state)

            # np.testing.assert(prev,state)
            prob.move_undo()
            np.testing.assert_array_equal(prev, prob.state)

            prob.move_undo()
            np.testing.assert_array_equal(state, prob.state)

            prev = state
