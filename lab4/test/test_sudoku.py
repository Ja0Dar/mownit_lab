from unittest import TestCase
from ex3.sudoku import Sudoku
import numpy as np
from copy import deepcopy

class TestSudoku(TestCase):
    def setUp(self):
        board = np.zeros((9, 9), dtype=int)
        self.prob = Sudoku(board)

    def test_energy_delta(self):
        e = self.prob._calculate_energy()

        for i in range(200):
            self.prob.move()
            e += self.prob._energy_delta()
            self.assertEqual(e, self.prob._calculate_energy())

    #todo improve on
    def test_energy(self):
        prob = self.prob
        prev_e = prob.energy()
        for i in range(300):
            prob.move()
            prob.move_undo()
            self.assertEqual(prev_e, prob.energy())

    def test_move_undo(self):
        prob = self.prob
        prev = deepcopy(prob.state)
        for i in range(300):
            prob.move()
            state = deepcopy(prob.state)

            # np.testing.assert(prev,state)
            prob.move_undo()
            np.testing.assert_array_equal(prev, prob.state)

            prob.move_undo()
            np.testing.assert_array_equal(state, prob.state)

            prev = state

    def test_fill_board(self):
        board = self.prob.state
        for i in range(1, 9):
            i_appearances = np.where(board[:] == i)[0].size
            self.assertEqual(i_appearances, 9, "Wrong number of appearances")
