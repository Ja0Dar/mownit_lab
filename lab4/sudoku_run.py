import argparse
from ex3.sudoku_io import load_sudoku,plot_sudoku
import  simulated_annealing as sa
import ex3.sudoku as su
import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-f','--filename', help='file with sudoku board as x** in ex3/boards', default="ex3/boards/xcm")
    args = parser.parse_args()

    fn=args.filename
    board = load_sudoku(fn)
    problem = su.Sudoku(board)
    res, e_arr = sa.simulated_annealing(sa.linear_dT(0.5, x0=6e5), problem, max_i=4e5, cycles=4)
    print("\n")
    print(res)
    if su.check_sudoku(res):
        print("Correct solution")
    else:
        e= np.min(e_arr[:,1])
        print("Loocal minimum E={}".format(e))
    plot_sudoku(board,res,e_arr)
