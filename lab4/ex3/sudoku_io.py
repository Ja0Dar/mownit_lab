import matplotlib.pyplot as plt
import pandas as pd
from ex2.image_io import plot_image_results


def plot_sudoku(start_board, end_board, energy_array, auto_show=True, title=None):
    plot_image_results(start_board, end_board, energy_array, auto_show, title)


# generate examples at
# https://qqwing.com/generate.html
def load_sudoku(filename):
    x = pd.read_csv(filename, header=None)
    x = x[0].apply(lambda row: pd.to_numeric(pd.Series(map(lambda x: 0 if x in ["x", "."] else x, list(row)))))
    return x.as_matrix()



