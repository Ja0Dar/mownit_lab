import numpy as np, pandas as pd
import os
import ex3.sudoku_io as io
from  ex3.sudoku import Sudoku, check_sudoku
import simulated_annealing as sa

if __name__ == '__main__':

    dirname = "boards"
    board_filenames = map(lambda filename: dirname + "/" + filename,
                          filter(lambda x: x != "boards", os.listdir(dirname)))


    board_fn_list = list(board_filenames)


    for fn in board_fn_list[:30]:
        board = io.load_sudoku(fn)
        problem = Sudoku(board)
        res, e_arr = sa.simulated_annealing(sa.linear_dT(0.5, x0=6e5), problem, max_i=4e5, cycles=4)
        # res, e_arr = sa.simulated_annealing(sa.square_dT(0.5), problem, max_i=4e4, cycles=4)
        # res, e_arr = sa.simulated_annealing(sa.default_dT(1, 0.01), problem, max_i=1e5, cycles=1)


        e = np.min(e_arr[:, 1])
        print("\n")
        print(res)
        if e > 0:
            print("{} fail, E= {}".format(fn, e))
        else:
            print("{} success, E= {}".format(fn, e))
        io.plot_sudoku(board, res, e_arr)
