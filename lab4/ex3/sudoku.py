import simulated_annealing as sa
from typing import Tuple
from copy import deepcopy
import numpy as np


class Sudoku(sa.Problem):
    def __init__(self, board) -> None:
        copied_board = deepcopy(board)
        self.fixed_points = self.get_fixed_points(copied_board)
        self.fill_board(copied_board)
        super().__init__(copied_board)
        self._prev_energy = None
        self._energy = self._calculate_energy()

    def move(self):

        self._col, self._row1, self._row2 = self._get_random_points()

        self._swap_state()

        self._prev_energy = self._energy
        self._energy = self._prev_energy+self._energy_delta()

    def move_undo(self):
        if self._prev_energy is None:
            raise Exception("cannot undo when no move was performed")

        self._swap_state()
        self._energy, self._prev_energy = self._prev_energy, self._energy

    def _get_random_points(self):
        col = np.random.randint(0, 9)
        r1 = np.random.randint(0, 9)
        r2 = np.random.randint(0, 9)
        while r1 == r2 or self.is_fixed((r1, col)) or self.is_fixed((r2, col)):
            col = np.random.randint(0, 9)
            r1 = np.random.randint(0, 9)
            r2 = np.random.randint(0, 9)
        return col, r1, r2

    def energy(self):
        return self._energy

    def _swap_state(self):
        self.state[[self._row1, self._row2], self._col] = self.state[[self._row2, self._row1], self._col]

    def _calculate_energy(self):
        e = 0
        for i in range(9):
            y0, x0 = square_upperleft_cords(i)
            e += 9 - np.unique(self.state[y0:y0 + 3, x0:x0 + 3]).size
            e += 9 - np.unique(self.state[i, :]).size
        return e

    def is_fixed(self, point: Tuple[int, int]):
        i, j = point
        return self.fixed_points[i, j]

    def _energy_delta(self):
        p1, p2 = (self._row1, self._col), (self._row2, self._col)

        energy_gained = self._from_point_energy(p1) + self._from_point_energy(p2)

        self.move_undo()

        lost_energy= self._from_point_energy(p1) + self._from_point_energy(p2)

        self.move_undo()
        # lost_energy = self._from_point_energy(p1, self.state[p2[0], p2[1]]) + \
        #               self._from_point_energy(p2, self.state[p1[0], p1[1]])

        return energy_gained - lost_energy

    def _from_point_energy(self, point: Tuple[int, int], value=None):

        row, col = point
        y0 = row - row % 3
        x0 = col - col % 3
        value = self.state[row, col] if value is None else value

        in_row = np.where(self.state[row] == value)[0].size
        in_square = np.where(self.state[y0:y0 + 3, x0:x0 + 3] == value)[0].size
        e = 0
        if self.state[row, col] == value:
            i = 1
        else:
            i = 0

        if in_row > i:
            e += 1
        if in_square > i:
            e += 1
        return e

    @staticmethod
    def get_fixed_points(board):
        n = board[0, :].size
        fixed_points = np.zeros((n, n), dtype=bool)

        for i in range(n):
            for j in range(n):
                if board[i, j] > 0:
                    fixed_points[i, j] = True

        return fixed_points

    def fill_board(self, board):
        for col in range(9):
            available_in_col = np.ones(10, dtype=bool)
            available_in_col[0] = False

            for row in range(9):
                available_in_col[board[row, col]] = False
            for row in range(9):
                if board[row, col] == 0:
                    i = np.where(available_in_col == True)[0][0]
                    board[row, col] = i
                    available_in_col[i] = False


def square_upperleft_cords(square_nr):
    row = (square_nr // 3) * 3
    col = (square_nr * 3) % 9
    return row, col


def check_sudoku(board):
    for i in range(9):
        row_i = board[i, :]
        col_i = board[:, i]
        y0, x0 = square_upperleft_cords(i)
        square_i = board[y0:y0 + 3, x0:x0 + 3]

        for pack in [row_i, col_i, square_i]:
            if np.unique(pack).size != 9:
                return False
    return True


