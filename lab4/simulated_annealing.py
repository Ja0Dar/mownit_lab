import math
import copy
import random
import numpy as np
import sys


def print_decor(f):
    def inner(*args, **kwargs):
        res = f(*args, **kwargs)
        print(res)
        return res

    return inner


class Problem():
    def __init__(self, state) -> None:
        super().__init__()
        self.state = state

    def energy(self):
        pass

    def move(self):
        pass

    def move_undo(self):
        pass


def default_dT(Tmax, Tmin):
    Tmax = np.float64(Tmax)
    Tmin = np.float64(Tmin)
    tfactor = -np.log(Tmax) + np.log(Tmin)

    def result(i, max_iter):
        return Tmax * np.exp(tfactor * i / max_iter)

    return result


# @print_decor
def linear_dT(Tmax, x0=None):
    if x0 is None:
        def poor_res(i, max_iter):
            factor = Tmax / max_iter
            return factor * (max_iter - i)

        return poor_res
    else:
        factor = Tmax / x0

        def better_res(i, *args):
            return Tmax - i * factor

        return better_res


def square_dT(Tmax):
    # @print_decor
    def res(i, max_iter):
        a = Tmax / np.power(max_iter, 2)
        return a * np.power(i - max_iter, 2)

    return res


def simulated_annealing(T_fun, prob: Problem, max_i: int = 1e5, iter_limit: int = None, E_interval=2, cycles=1,
                        acceptable_energy=0):
    E = prev_E = prob.energy()
    max_i = np.int32(max_i)

    # max_i defines curve, iterlimit defines cutout point
    iter_limit = max_i if iter_limit is None else np.int32(iter_limit)
    i = np.int32(0)
    T = T_fun(i, max_i)

    e_i = 0
    e_points = np.ones((int(iter_limit * cycles / E_interval) + 1, 2)) * -1

    best_state = copy.copy(prob.state)
    best_energy = E

    iter_per_1pp = iter_limit // 100
    while i < iter_limit:

        if i % iter_per_1pp == 0:
            sys.stdout.write("\r{}%".format(int(i / iter_per_1pp)))
        for c in range(cycles):
            if (i * cycles + c) % E_interval == 0:
                e_points[e_i, :] = i * cycles + c, prev_E
                e_i += 1
            prob.move()
            E = prob.energy()

            if E == acceptable_energy:
                e_points[e_i, :] = i, acceptable_energy
                return prob.state, e_points[np.where(e_points[:, 0] >= 0)]

            dE = E - prev_E

            if dE > 0.0 and np.exp(-dE / T) < random.random():
                # Reject and
                # Restore previous state
                prob.move_undo()
                E = prev_E
            else:
                prev_E = E
                if E < best_energy:
                    best_state = copy.copy(prob.state)
                    best_energy = E
        i += 1
        T = T_fun(i, max_i)

    e_points = e_points[np.where(e_points[:, 0] >= 0)]

    if best_energy < E:
        return best_state, e_points
    return prob.state, e_points
