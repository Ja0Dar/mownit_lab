import numpy as np
from mpl_toolkits.mplot3d.axes3d import Axes3D
import matplotlib.pyplot as plt


def generate_sphere(r):
    step = 1e-1
    phi = np.arange(0, 2 * np.pi, step)
    theta = np.arange(-np.pi, np.pi, step)

    A = np.zeros((phi.size * theta.size, 3))

    i = 0
    for p in phi:
        for t in theta:
            A[i, 0] = r * np.cos(p) * np.cos(t)
            A[i, 1] = r * np.sin(p) * np.cos(t)
            A[i, 2] = r * np.sin(t)
            i += 1
    return A

def plot_3d(A):
    fig = plt.figure()
    ax = fig.gca(projection="3d")
    ax.scatter(A[:,0], A[:,1], A[:,2])
    plt.show()



if __name__ == '__main__':
    a= generate_sphere(1)
    plot_3d(a)