function [x,i] = newton(fun,fprim,a,b,abs_err,prec)
  old_digits = digits();
  n=200;
  digits(prec);
  a=vpa(a);
  b=vpa(b);
  
  abs_err=vpa(abs_err);
  
  delta= vpa(1e-20);
  
  ya= fun(a);
  yb=fun(b);
  xPrev=a;
  x=b;
  xVal=ya;
  i=0;
   while (n>i && abs(xPrev-x)>abs_err)
     i+=1;
    xPrev=x;
    x=xPrev-xVal/fprim(x);
    xVal=fun(x);
    if xVal ==0
      disp("accidently zeroead");
      return
    endif   
   endwhile
    digits(old_digits);
  
endfunction