function [c,i] = Bisect(fun,a,b,abs_err,prec)
  old_digits = digits();
  
  n= ceil(log((b-a)/abs_err)/log(2))+1;
  
  digits(prec);
  a=vpa(a);
  b=vpa(b);
  
  abs_err=vpa(abs_err);
  
  delta= vpa(1e-20);
  
  ya= fun(a);
  yb=fun(b);
  c=a;
  yc=ya;
  
  range= vpa(50);
  
  % initial find diffrent sign:
  if sign(ya) == sign(yb)    
    for c=a:(b-a)/range:b
      yc= fun(c);
      if sign(yc) !=sign(ya)
        a=c;
        break;
      endif
    endfor
  endif
  
  
  
    i=0;
   while ( abs(yc)>abs_err &&  abs(b-a)>delta)
   #while ( i<=n &&  abs(b-a)>delta)
    i=i+1;
    c= a+ (b-a)/2;
    yc= fun(c);
    if yc ==0
      disp("accidently zeroead");
      return
    endif
    
    if sign(ya)==sign(yc)
      ya=yc;
      a=c;
    elseif sign(yb)== sign(yc)
      yb=yc;
      b=c;
    else
      error("sgn(a) != sgn(b) - error")
    endif
   
   
   endwhile
    digits(old_digits);
  
endfunction