pkg load symbolic


abs_err=1e-8;
prec = 32;

f1= @(x) cos(x).*cosh(x)-1;
f1_prim =@(x) cos(x)*sinh(x)-cosh(x)*sin(x);
a1=3*pi/2;
b1=2*pi;


[bis1,i]=Bisect(f1,a1,b1,abs_err,prec)
[newt1,i]=newton(f1,f1_prim,a1,b1,abs_err,prec)
[secant1,i]=secant(f1,a1,b1,abs_err,prec)
zero1=f1([bis1,newt1,secant1])
disp("\n\n\n\n---------------");

f2= @(x) 1./x -tan(x);
f2_prim =@(x) -1/x^2 -(sec(x))^2;
a2=1e-20;
b2= pi/2;

[bis2,i]=Bisect(f2,a2,b2,abs_err,prec)
[newt2,i]=newton(f2,f2_prim,a2,b2,abs_err,prec)
[secant2,i]=secant(f2,a2,b2,abs_err,prec)
zero2=f2([bis2,newt2,secant2])
disp("\n\n\n\n---------------");


f3= @(x) 2.^-x + exp(x)+ 2.*cos(x) -6;
f3_prim = @(x) exp(x) - (2.^(-x))*vpa(log(2)) -2*sin(x);
a3=1;
b3=3;
bis3=Bisect(f3,a3,b3,abs_err,prec)
newt3=newton(f3,f3_prim,a3,b3,abs_err,prec)
secant3=secant(f3,a3,b3,abs_err,prec)
zero3=f3([bis3,newt3,secant3])

