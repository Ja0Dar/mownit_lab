N=503;
A = randn(N,N);
y = randn(N,1);



start = time();
gauss_jord=gauss_jordan(A,y);
finish = time();
myTime = finish-start

start = time();
linsolve_result = linsolve(A,y);
finish = time();
linsolveTime = finish-start



start = time();
mldivide_result = mldivide(A,y);
finish = time();
mldivideTime = finish-start



start = time();
left_div_result = A\y;
finish = time();
leftDivideTime = finish-start


squares=(gauss_jord-linsolve_result).^2;
mean_error = mean(squares(:))