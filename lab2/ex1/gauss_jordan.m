function [x] = gauss_jordan (A, y)
  %A*x = y
N = size(y,1);
A = [A y];  %extended matrix

%ROW SCALING : 
for k =1:N
  maxInRow = max(abs(A(k,:)));
  A(k,:)= A(k,:)./maxInRow;
end


%PIVOTING
for k =1:N
  [maxVal,imaxVal] = max(abs(A(k:end,k)));  % max value on or below diagonal
  imaxVal= k + imaxVal -1;
  if maxVal==0
     error("Singuar matrix")
   endif
   
   
   if imaxVal != k
     A([k imaxVal],:) = A([imaxVal k],:);
    endif
    
    
   %cleaning non-diagonal elements
   for row=1:N
     if row != k
        lambda = A(row,k)/A(k,k); 
        A(row,:) = A(row,:) - lambda* A(k,:);
      endif
     endfor

end
  
 %Substitution 
 x= zeros(N,1);
 for k = 1:N
  x(k) = A(k,N+1)/A(k,k);
 endfor
endfunction
