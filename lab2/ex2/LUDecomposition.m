% L- lower triangle matrix, U - upper triangle matrix, P - permutation matrix, S - scale matrix.
% when using row scaling:
%  S*inv(P)*L*U=A

function [L,U,P,S] = LUDecomposition (A)
  if(det(A) ==0)
   error("Cannot decompose singular matrix "); 
  endif
  
  N = size(A,1);
  L=eye(N); % dolittle method - L has to have ones on diagonal 
  S= eye(N);
  P= eye(N);

%row scaling
  for k= 1:N 
   S(k,k) = max(abs(A(k,:)));
   A(k,:)./= S(k,k);
  endfor
  
  %Partial pivoting
  for k=1:N
    [~,imaxInCol] = max(abs(A(k:end,k)));  % max value on or below diagonal
    imaxInCol= k + imaxInCol -1;
    % A[imaxInCol,k] != 0, because det(A) !=0
    
    
     if imaxInCol != k
       A([k, imaxInCol],:) = A([imaxInCol k],:);
       P([k, imaxInCol],:) = P([imaxInCol k],:);
      endif    
  endfor

  %Fill L matrix and clean lower part of A 
  for k =1:N
     for row=k+1:N
          lambda = A(row,k)/A(k,k); 
          A(row,:) = A(row,:) - lambda* A(k,:);
          L(row,k) = lambda;
          
       endfor
  endfor
    U=A;
endfunction
