
function [L,U,row_indices] = LUDecompositionWithoutScaling (A)
  N = size(A,1); % - size(A,2);
  if(det(A) ==0)
   error("Cannot decompose singular matrix "); 
   endif
  L=eye(N); 
  row_indices = (1:N)';


for k=1:N
  %PIVOTING
  [~,imaxInCol] = max(abs(A(k:end,k)));  % max value on or below diagonal
  imaxInCol= k + imaxInCol -1;
  
   % row swapping
   if imaxInCol != k
     A([k, imaxInCol],:) = A([imaxInCol k],:);
     row_indices([k, imaxInCol],:) = row_indices([imaxInCol k],:);
     
    end    
end

for k =1:N
  
   %clean rows below. 
   for row=k+1:N
        lambda = A(row,k)/A(k,k); 
        A(row,:) = A(row,:) - lambda* A(k,:);
        L(row,k) = lambda;
        
     end
  
    
 
end

  U=A;
endfunction