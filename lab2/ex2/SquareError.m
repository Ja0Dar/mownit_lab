function error = SquareError(A1,A);
  sqr= (A1-A).^2;
  
  error= mean(sqr(:));
  
  
  endfunction