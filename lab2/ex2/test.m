N=300;
high_row_multiplier = 1e14
low_row_multiplier=1e-15

A = randn(N);

for i=1:N
  if mod(i,4) ==0   
    A(i,:)= A(i,:).*high_row_multiplier;    
  elseif mod(i,7) == 0  
   A(i,:)= A(i,:).*low_row_multiplier;   
  endif
 endfor

 
showErrors(A);