function [with_scaling_error,without_scaling_error] = showErrors(A) 

% comparing errors with rescaled matrices.  
[L,U,P,S] = LUDecomposition(A);
[L2,U2,indices2] = LUDecompositionWithoutScaling(A);

with_scaling_error= SquareError(S*inv(P)*L*U,A)
downscaled_with_scaling_error = SquareError(L*U,P*inv(S)*A)

A1=L2*U2;
A=A(indices2,:);

without_scaling_error = SquareError(A1,A)

for i=1:size(A1,1)
  A1(i,:)./= max(A1(i,:));
  A(i,:)./= max(A(i,:));
  endfor
downscaled_without_scaling_error = SquareError(A1,A)
  
  endfunction