import copy
import numpy as np
import networkx as nx


def kirchhoff_analysis(s, t, EMF, G):
    ordered_edge_list = copy.deepcopy(list(G.edges))  # because G.edges sometimes doesn't keep order

    A, y = get_kirchhoff_matrices(s, t, EMF, G, ordered_edge_list)
    x = np.linalg.lstsq(A, y)[0]

    G2 = nx.DiGraph()
    for edge_i, edge in enumerate(ordered_edge_list):
        i, j = edge
        I = float(x[edge_i])
        R = float(G[i][j]["R"])

        if I < 0:
            G2.add_edge(j, i, I=-I, R=R)
        else:
            G2.add_edge(i, j, I=I, R=R)
    return G2


def get_kirchhoff_matrices(s, t, EMF, G, ordered_edge_list):
    # assumption : I is positive if i<j  and i -flows-into-> j
    # incoming flows are + out are -

    V = len(G.nodes)
    E = len(G.edges)
    iv_index = E

    paths = find_all_paths(s, t, G)
    N = len(paths)
    A = np.zeros((N + V, E + 1))
    y = np.zeros((N + V, 1))

    def edge_i(i, j):
        if i > j:
            i, j = j, i
        return ordered_edge_list.index((i, j))

    # KVL
    for path_i, path in enumerate(paths):
        y[path_i] = EMF
        for i, j in path:
            if i < j:
                A[path_i, edge_i(i, j)] = G[i][j]["R"]
            else:
                A[path_i, edge_i(i, j)] = -G[i][j]["R"]

    # KCL
    for node in range(0, V):
        vertical_i = N + node
        # iV is always oriented t-> s
        if node == t:
            A[vertical_i, iv_index] = -1
        elif node == s:
            A[vertical_i, iv_index] = 1

        for nghb in nx.neighbors(G, node):
            # I direction : nghb <---  node
            if nghb > node:
                e_i = edge_i(node, nghb)
                A[vertical_i, e_i] = -1
            # I direction : nghb ---> node
            else:
                e_i = edge_i(nghb, node)
                A[vertical_i, e_i] = 1
    return A, y


def find_all_paths(start, end, G):
    res = []

    def find_paths_inner(current: int, path_end: int, prev_edges: list, visited_nodes: set, result: list):
        visited_nodes.add(current)

        for nghb in G[current]:
            if nghb == path_end:
                part_res = copy.copy(prev_edges)
                part_res.append((current, nghb))
                result.append(part_res)
            elif not (nghb in visited_nodes):
                new_visited_nodes = copy.copy(visited_nodes)

                new_prev_edges = copy.copy(prev_edges)
                new_prev_edges.append((current, nghb))

                find_paths_inner(nghb, path_end, new_prev_edges, new_visited_nodes, result)

    find_paths_inner(start, end, [], set(), res)
    return res
