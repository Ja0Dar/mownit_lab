import sys
from graph_generator import ResistorGraphGenerator
from kirchhoff import kirchhoff_analysis
from nodal_analysis import nodal_analysis
from  networkx import write_gexf

gen = ResistorGraphGenerator(4, 20)


def random():
    fun_name = sys._getframe().f_code.co_name
    g = gen.connected_random(n=15, p=0.3)
    s = 0
    t = 5
    EMF = 30

    analyze_and_save(s, t, EMF, g, fun_name)


def cubic():
    fun_name = sys._getframe().f_code.co_name
    g = gen.cubic()
    s = 0
    t = 5
    EMF = 30

    analyze_and_save(s, t, EMF, g, fun_name)


def bridge():
    fun_name = sys._getframe().f_code.co_name
    g = gen.bridge(8, 8, 0.35)
    s = 0
    t = 12
    EMF = 600
    analyze_and_save(s, t, EMF, g, fun_name)


def grid():
    fun_name = sys._getframe().f_code.co_name
    g = gen.grid_2d(4, 6)
    s = 9
    t = 23
    EMF = 60
    analyze_and_save(s, t, EMF, g, fun_name)


def write_kir_gexf(name, g):
    write_gexf(g, name + "_kir.gexf")


def write_nod_gexf(name, g):
    write_gexf(g, name + "_nod.gexf")


def analyze_and_save(s, t, EMF, g, fun_name):
    kir = kirchhoff_analysis(s, t, EMF, g)
    nod = nodal_analysis(s, t, EMF, g)

    compare_methods(kir, nod, True)
    write_kir_gexf(fun_name, kir)
    write_nod_gexf(fun_name, nod)


def compare_methods(kir, nod, debug=False):
    eps = 1e-5
    result = True
    for i, j in kir.edges:
        if abs(kir[i][j]["I"]) > eps:
            i_kir = kir[i][j]["I"]
            try:
                i_nod = nod[i][j]["I"]
            except KeyError:
                if debug:
                    print("Reversed")
                    result = False
                else:
                    return False
                i_nod = nod[j][i]["I"]
            if abs(i_kir - i_nod) > eps:
                if debug:
                    print(i, j, i_kir, i_nod)
                    result = False
                else:
                    return False

    return result


def nodal_random():
    g = gen.connected_random(80, p=0.3)
    nod = nodal_analysis(1, 78, 1000, g)
    write_gexf(nod, "big_random.gexf")


if __name__ == '__main__':
    random()
    print("Random saved", flush=True)
    bridge()
    print("Bridge saved", flush=True)
    grid()
    print("Grid saved", flush=True)
    cubic()
    print("Cubical saved", flush=True)

    nodal_random()
