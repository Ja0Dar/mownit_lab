import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt


def load_graph_from_file(filename, v1_label="V1", v2_label="V2", resistance_label="R"):
    data = pd.read_csv(filename)
    return load_graph_from_pd(data, v1_label, v2_label, resistance_label)


def load_graph_from_pd(data, v1_label, v2_label, resistance_label):
    G = nx.Graph()
    for i in range(0, data.__len__()):
        G.add_edge(data[v1_label][i], data[v2_label][i], R=data[resistance_label][i])
    return G


def display_graph(G):
    nx.draw(G, with_labels=True, arrows=True)
    plt.show()


# something is wrong with placing of  those labels :X
def display_weighted_graph(G, edge_label="R"):
    nx.draw(G, with_labels=True, arrows=True)
    pos = nx.spring_layout(G)
    labels = nx.get_edge_attributes(G, edge_label)
    nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)
    plt.show()


def save_graph_to_pd(G: nx.Graph, v1_label: str = "V1", r_label: str = "R", v2_label: str = "V2") -> pd.DataFrame:
    column_labels = [v1_label, r_label, v2_label]
    frame = pd.DataFrame(columns=column_labels)
    for i, edge in enumerate(G.edges):
        v1, v2 = edge
        R = G[v1][v2]["R"]
        frame.loc[i] = [v1, R, v2]
    return frame


def save_graph_to_file(G: nx.Graph, filename: str):
    data_frame = save_graph_to_pd(G)
    data_frame.to_csv(filename)
