import os.path as path
import argparse as ap
from networkx import write_gexf
from utils import load_graph_from_file

if __name__ == '__main__':
    parser = ap.ArgumentParser()
    parser.add_argument("filename",
                        help="CSV with columns V1,R,V2 where V1,V2 are "
                             "node labels and R is resistance on edge between them"
                             "node labels should be from 0 to N without gaps.")
    parser.add_argument("-s", "--source", type=int, help="Label of source node")
    parser.add_argument("-t", "--termination", type=int, help="Label of termination (ground) node")
    parser.add_argument("-E", "--EMF", type=int, help="Voltage between source and ground")
    parser.add_argument("-k", "--kirchhoff", action="store_true",
                        help="Use kirchhoff analysis instead of nodal analysis [default]")
    arg = parser.parse_args()

    filename = arg.filename
    if not path.exists(filename):
        print("{} does not exist".format(filename))
        exit(1)

    s = arg.source if arg.source is not None else int(input("Source node:"))
    t = arg.termination if arg.termination is not None else int(input("Termination/ground node:"))
    EMF = arg.EMF if arg.EMF is not None else int(input("Voltage between source and ground (int):"))

    g = load_graph_from_file(arg.filename)
    print("Graph loaded")

    if arg.kirchhoff:
        from kirchhoff import kirchhoff_analysis

        print("Performing kirchhoff analysis")
        g2 = kirchhoff_analysis(s, t, EMF, g)
    else:
        from nodal_analysis import nodal_analysis

        print("Performing nodal analysis")
        g2 = nodal_analysis(s, t, EMF, g)

    print("Graph analysed")

    split_fn = filename.split(".")
    if split_fn[-1] == "csv":
        filename = ".".join(split_fn[: -1])
    filename += ".gexf"
    write_gexf(g2, filename)

    print("Graph saved in {}".format(filename))
