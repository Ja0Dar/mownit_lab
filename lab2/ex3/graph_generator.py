from random import random, randint
import networkx as nx


def add_r_to_edges(G: nx.Graph, min_r: int, max_r: int) -> nx.Graph:
    for i, j in G.edges:
        G[i][j]["R"] = randint(min_r, max_r)
    return G


def with_resistance(func):
    def wrapper(*args, **kwargs):
        self = args[0]
        g = func(*args, **kwargs)
        return add_r_to_edges(g, self.min_r, self.max_r)

    return wrapper


class ResistorGraphGenerator:
    def __init__(self, min_r: int = 1, max_r: int = 100) -> None:
        super().__init__()
        self.min_r = min_r
        self.max_r = max_r

    @with_resistance
    def connected_random(self, n: int = 10, p: float = 0.4, n_start=0):
        G = nx.Graph()
        for i in range(n_start, n - 1):
            mandatory_nghb = randint(n_start, n - 1)
            while mandatory_nghb == i:
                mandatory_nghb = randint(n_start, n - 1)
            G.add_edge(i, mandatory_nghb)
            for j in range(i + 1, n):
                if j != mandatory_nghb:
                    if random() <= p:
                        G.add_edge(i, j)
        return G

    @with_resistance
    def cubic(self):
        # return nx.cubical_graph()
        return nx.dodecahedral_graph()

    @with_resistance
    def grid_2d(self, n: int, m: int):
        return nx.convert_node_labels_to_integers(nx.grid_2d_graph(m, n))

    # no decorator here - it is already in connected_random.
    def bridge(self, n: int, m: int,p=0.8):
        g1 = self.connected_random(n, p=p)
        g2 = self.connected_random(n=n + m, p=p, n_start=n)

        v1 = randint(0, n - 1)
        v2 = randint(n, n + m - 1)

        g = nx.compose(g1, g2)
        g.add_edge(v1, v2, R=randint(0, 100))
        return g


if __name__ == '__main__':
    from  utils import display_weighted_graph, save_graph_to_pd, load_graph_from_file, save_graph_to_file

    g = ResistorGraphGenerator(1, 100).connected_random(3)
    g = load_graph_from_file("graf.csv")
    display_weighted_graph(g, "R")
    save_graph_to_file(g, "graf.csv")
