import networkx as nx
import numpy as np

# As explained in :
# https://www.swarthmore.edu/NatSci/echeeve1/Ref/mna/MNA2.html
def fill_matrix_and_vector(s, t, E, G):
    iV_mi = t  # index of column for current flowing through voltage source

    # N-1 equations for nodes (omitting ground) + 1 equation for Voltage source
    N = len(G.nodes)
    A = np.zeros((N, N))
    y = np.zeros((N, 1))
    for node in G.nodes():
        if node == t:
            # omit equations for ground node, put equation for voltage source v(s)=E
            A[node, s] = 1
            y[node] = E
            continue
        if node == s:
            A[node, iV_mi] = 1

        for nghb in nx.neighbors(G, node):
            R_inversed = 1 / G[node][nghb]["R"]
            A[node][node] += R_inversed
            if nghb != t:
                A[node][nghb] -= R_inversed

    return A, y


def create_output_graph(G, v):
    G2 = nx.DiGraph()
    for i, j in G.edges:
        # flow from higher to lower voltage
        if v[i] < v[j]:
            i, j = j, i
        R = float(G[i][j]["R"])
        I = float((v[i] - v[j]) / R)
        G2.add_edge(i, j, R=R, I=I)
    return G2


def nodal_analysis(s, t, E, G):
    A, y = fill_matrix_and_vector(s, t, E, G)
    v = np.linalg.solve(A, y)
    v[t] = 0
    return create_output_graph(G, v)
