from typing import Tuple
from scipy.sparse import spmatrix
from engine import k_list
from engine.prepare_data import load_pretty_stemmed_articles, load_dict_reverse_dict, load_sparse, regular_matrix_fn, \
    idf_matrix_fn, \
    idf_normalized_matrix_fn, load_low_rank_usv
import logging


class Repository():
    def __init__(self):
        logging.info("initializing")
        self.pretty, self.stemmed = load_pretty_stemmed_articles()
        logging.info("Loaded {} articles".format(self.pretty.size()))
        self.dictionary, self.rev_dict = load_dict_reverse_dict()
        logging.info("Loaded dict with {} words ".format(len(self.dictionary)))
        self.A = load_sparse(regular_matrix_fn)
        self.B = load_sparse(idf_matrix_fn)
        self.C = load_sparse(idf_normalized_matrix_fn)
        logging.info("loaded sparse matrices ")
        for k in k_list:
            self.D(k)
        logging.info("loaded svd matrices")

    def D(self, k) -> Tuple[spmatrix, spmatrix, spmatrix]:
        d = {}
        if k not in d.keys():
            d[k] = load_low_rank_usv(k)
        return d[k]
