import numpy as np
from scipy.sparse import spmatrix, eye
import scipy.sparse.linalg as lin

from engine.initial_parsing import line_into_words
from engine.matrix_building import build_bow_from_reverse_dict
from engine.stemming import stem
import logging


def bag_of_words(sentence, rev_dict):
    bow = build_bow_from_reverse_dict(rev_dict, line_into_words(sentence).map(lambda w: stem(w)))
    return bow


def search_with_normalization(A: spmatrix, q: spmatrix):
    q = q.transpose().tocsr()
    A = A.tocsc()
    q_norm = lin.norm(q)
    norms = lin.norm(A, axis=0)
    correlation = q.dot(A).toarray() / norms / q_norm
    return np.flipud(np.argsort(correlation[0])), correlation[0]


def search_without_normalization(A: spmatrix, q: spmatrix) -> np.array:
    q = q.transpose().tocsr()
    A = A.tocsc()
    correlation = q.dot(A).toarray()
    return np.flipud(np.argsort(correlation[0])), correlation[0]


def low_rank_search(U: spmatrix, s: spmatrix, Vt: spmatrix, col_norm: np.array, q: spmatrix) -> np.array:
    q = q.transpose().tocsr()
    U = U.tocsr()
    s = eye(max(s.shape)).multiply(s)
    logging.log(logging.DEBUG, "Searched svd's k value : {}".format(s.shape[0]))
    Vt = Vt.tocsr()
    q_norm = lin.norm(q)
    correlation = q.dot(U).dot(s).dot(Vt).toarray() / col_norm / q_norm
    return np.flipud(np.argsort(correlation[0])), correlation[0]
