from functools import lru_cache
from nltk.stem.porter import PorterStemmer

port = PorterStemmer()


@lru_cache(maxsize=50000)
def stem(x):
    return port.stem(x)
