import os
import pickle
from typing import Tuple

import scipy.sparse as sparse
import scipy.linalg as lin
import numpy as np
from functional import seq

from engine.initial_parsing import pretty_stemmed_articles_from_chopped_articles, build_dict_reverse_dict, \
    chopped_articles_from_lines
from engine.matrix_building import build_matrix, build_idf_matrix, normalize_matrix_columns, low_rank_approximation

source_dir = "data/dumps"
generated_dir = "data/generated"
stemmed_fn = "stemmed_articles.txt"
pretty_fn = "pretty_articles.txt"
dict_fn = "dictionary.txt"
reverse_dict_fn = "reverse_dictionary.txt"
regular_matrix_fn = "A.npz"
idf_matrix_fn = "B.npz"
idf_normalized_matrix_fn = "C.npz"


def low_rank_fn_suffix(k: int, ext="npz"):
    return "_low_rank_{}.{}".format(k, ext)


def dump(x, filename):
    file = open(generated_dir + "/" + filename, "wb")
    pickle.dump(x, file)
    file.close()


def load(filename):
    file = open(generated_dir + "/" + filename, "rb")
    res = pickle.load(file)
    file.close()
    return res


def prepare_pretty_stemmed_articles() -> Tuple[seq, seq]:
    lines = []
    for filename in os.listdir(source_dir):
        lines += open("{}/{}".format(source_dir, filename)).readlines()

    chopped_articles = chopped_articles_from_lines(lines)
    pretty, stemmed = pretty_stemmed_articles_from_chopped_articles(chopped_articles)
    pretty_serializable = list(pretty)
    stemmed_serializable = list(stemmed.map(lambda x: list(x)))

    dump(stemmed_serializable, stemmed_fn)
    dump(pretty_serializable, pretty_fn)

    return pretty, stemmed


def load_pretty_stemmed_articles() -> Tuple[seq, seq]:
    stemmed = load(stemmed_fn)
    pretty = load(pretty_fn)

    return seq(pretty), seq(stemmed).map(lambda x: seq(x))


def prepare_dict_rev_dict(stemmed):
    dictionary, reverse_dict = build_dict_reverse_dict(stemmed)
    dump(dictionary, dict_fn)
    dump(reverse_dict, reverse_dict_fn)
    return dictionary, reverse_dict


def load_dict_reverse_dict():
    d = load(dict_fn)
    rd = load(reverse_dict_fn)
    return d, rd


def save_sparse(filename: str, array: sparse.spmatrix) -> None:
    if sparse.isspmatrix_dok(array) or sparse.isspmatrix_lil(array):
        array = array.tocsr()
    sparse.save_npz(generated_dir + "/" + filename, array)


def load_sparse(filename) -> sparse.spmatrix:
    return sparse.load_npz(generated_dir + "/" + filename)


def save_low_rank(k, U, s, V_trans, col_norm):
    save_sparse("U" + low_rank_fn_suffix(k), U)
    save_sparse("s" + low_rank_fn_suffix(k), s)
    save_sparse("Vh" + low_rank_fn_suffix(k), V_trans)
    np.save(generated_dir + "/col_norm" + low_rank_fn_suffix(k, ext="npy"), col_norm)


def load_low_rank_usv(k) -> Tuple[sparse.spmatrix, sparse.spmatrix, sparse.spmatrix, np.array]:
    U = load_sparse("U" + low_rank_fn_suffix(k))
    s = load_sparse("s" + low_rank_fn_suffix(k))
    V_trans = load_sparse("Vh" + low_rank_fn_suffix(k))
    col_norm = np.load(generated_dir + "/col_norm" + low_rank_fn_suffix(k, ext="npy"))
    return U, s, V_trans, col_norm


def prepare_regular_matrix(rev_dict, stemmed) -> sparse.spmatrix:
    A = build_matrix(rev_dict, stemmed)
    print("matrix prepared, now saving")
    save_sparse(regular_matrix_fn, A)
    print("matrix saved")
    return A


def prepare_idf_matrix(regular_matrix: sparse.spmatrix):
    B = build_idf_matrix(regular_matrix)
    save_sparse(idf_matrix_fn, B)
    return B


def prepare_normalized_idf_matrix(idf_matrix: sparse.spmatrix):
    C = normalize_matrix_columns(idf_matrix)
    save_sparse(idf_normalized_matrix_fn, C)
    return C


def prepare_low_rank_usv(idf_normalized: sparse.spmatrix, k: int):
    u, s, vh = low_rank_approximation(idf_normalized, k)
    Ak = u.dot(sparse.eye(s.shape[1]).multiply(s)).dot(vh).todense()
    norm = lin.norm(Ak, axis=0)

    save_low_rank(k, u, s, vh, norm)
    return u, s, vh
