from typing import Tuple, List, Dict

from functional import seq
import string
from nltk.corpus import stopwords

from engine.stemming import stem

alphanumeric = string.ascii_letters + string.digits
alphanumeric_lower = string.ascii_lowercase + string.digits


def allowed_word(word):
    return all(c in alphanumeric for c in word)


def allowed_word_lower(word):
    return all(c in alphanumeric_lower for c in word)


def split(txt, seps):
    translation_table = str.maketrans(seps, ' ' * len(seps))
    return str.translate(txt, translation_table).split()


def line_into_words(line: str):
    punctuation = string.punctuation + "–"

    return seq(
        split(line, punctuation)
    ).filter(lambda x: len(x) > 1)


def chopped_articles_from_lines(lines):
    articles = []
    tmp_article = []
    for line in lines:
        if line.startswith("[[") and (line.endswith("]]") or line.endswith("]]\n")):
            line = line[2:-3]
            articles.append(tmp_article)
            tmp_article = []
        tmp_article.append(line)
    articles.append(tmp_article)

    return seq(articles[1:]).map(lambda art: seq(art))


def pretty_stemmed_articles_from_chopped_articles(articles) -> Tuple[seq, seq]:
    pretty_articles = articles.map(lambda art: " ".join(art))

    # using set for faster lookup
    stop_set = set(stopwords.words("english"))

    stemmed = articles.map(
        lambda article: article
            .flat_map(lambda line: line_into_words(line))
            .filter(lambda word: word not in stop_set and allowed_word(word))
            .map(lambda word: stem(word)))

    return pretty_articles, stemmed


def build_dictionary(stemmed_articles) -> List[str]:
    res = set()

    stemmed_articles.for_each(lambda article: res.update(article))
    return sorted(list(
        res
    ))


def build_dict_reverse_dict(stemmed_articles) -> Tuple[List[str], Dict[str, int]]:
    d_set = set()
    stemmed_articles.for_each(lambda article: d_set.update(article))

    dictionary = list(d_set)
    reverse_dict = dict()
    for i, word in enumerate(dictionary):
        reverse_dict[word] = i

    return dictionary, reverse_dict
