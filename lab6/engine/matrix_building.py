from collections import Counter
from copy import copy
from typing import List, Tuple, Dict

import numpy as np
import scipy.sparse.linalg as lin
from functional import seq
from scipy.sparse import spmatrix, lil_matrix, csc_matrix, csr_matrix


def wordcount_dict(words):
    c = Counter()
    for word in words:
        c[word] += 1
    return c


def list_index(l: List, elem):
    try:
        return l.index(elem)
    except ValueError:
        return -1


def build_bow_from_dict(dictionary, stemmed_words):
    vec = lil_matrix((len(dictionary), 1), dtype=np.float64)
    for word, word_count in wordcount_dict(stemmed_words).items():
        word_index = list_index(dictionary, word)
        if word_index > -1:
            vec[word_index, 0] = word_count
    return vec


def build_bow_from_reverse_dict(rev_dict, stemmed_words):
    vec = lil_matrix((len(rev_dict), 1), dtype=np.float64)
    for word, word_count in wordcount_dict(stemmed_words).items():
        if word in rev_dict:
            vec[rev_dict[word]] = word_count
    return vec


def build_matrix(rev_dict: Dict[str, int], stemmed_articles: seq):
    A = lil_matrix((len(rev_dict), stemmed_articles.size()), dtype=np.float64)
    for j, article in enumerate(stemmed_articles):
        for word, occurrences in wordcount_dict(article).items():
            if word in rev_dict:
                i = rev_dict[word]
                A[i, j] = occurrences
    return A.tocsc()


def build_idf_matrix(regular_matrix: spmatrix):
    B = copy(regular_matrix).tocsr()
    dict_len, article_no = B.shape
    for word_i in range(dict_len):
        idf = np.log(article_no / B[word_i, :].nonzero()[0].size)
        B[word_i, :] = idf * B[word_i, :]
    return B


def normalize_column(col: spmatrix):
    return col / lin.norm(col)


def normalize_matrix_columns(B: spmatrix):
    B = B.tocsc()
    for j in range(B.shape[1]):
        B[:, j] = normalize_column(B[:, j])
    return B


def low_rank_approximation(A: spmatrix, k=5) -> Tuple[spmatrix, spmatrix, spmatrix]:
    U, s, V_trans = lin.svds(A, k, tol=1e-10)
    s = csr_matrix(s)
    U = csc_matrix(U)
    V_trans = csr_matrix(V_trans)
    return U, s, V_trans
