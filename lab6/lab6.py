from functools import lru_cache

from copy import copy
from flask import Flask

from engine.matrix_building import normalize_column
from engine.searching import bag_of_words, search_without_normalization, search_with_normalization, low_rank_search
from web.forms import SearchForm
from flask import render_template, request, redirect
from urllib.parse import quote, unquote

from engine import Repository, k_list

import logging

logging.getLogger().setLevel(logging.DEBUG)
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
    search = SearchForm(request.form)
    if request.method == 'POST':
        return redirect("results/{}/{}".format(quote(search.data['select']), quote(search.data['search'])), 303, None)

    return render_template('index.html', form=search)


@app.route("/results/<search_type>/<query>")
def results(search_type, query):
    search_type = unquote(search_type)
    query = unquote(query)

    res, correl = search_bow(search_type, query)

    return render_template('results.html', results=res, correl=correl, pretty=loaded.pretty)


@app.route("/articles/<int:id>/pretty")
def pretty_route(id):
    return render_template('pretty.html', text=loaded.pretty[id].split("\n"))


@app.route("/articles/<int:id>/stemmed")
def stemmed_route(id):
    return render_template('stemmed.html', stemmed=loaded.stemmed[id], id=id)


@lru_cache(maxsize=5000)
def search_bow(search_id: str, query):
    bow_in = bag_of_words(unquote(query), loaded.rev_dict)
    d = {
        'basic': lambda bow: search_without_normalization(loaded.A, bow),
        'basic_norm': lambda bow: search_with_normalization(loaded.A, bow),
        'idf': lambda bow: search_without_normalization(loaded.B, bow),
        'idf_norm': lambda bow: search_without_normalization(loaded.C, normalize_column(bow)),  # C = normalized B
    }
    if search_id[0] == "D":
        return low_rank_search(*loaded.D(int(search_id[1:])), bow_in)

    return d[search_id](bow_in)


if __name__ == '__main__':
    loaded = Repository()
    app.run()
