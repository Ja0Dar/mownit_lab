from engine import k_list
from engine.prepare_data import *
import nltk

if __name__ == '__main__':
    nltk.download("stopwords")

    os.makedirs(generated_dir, exist_ok=True)
    os.makedirs(source_dir, exist_ok=True)

    if not os.listdir(source_dir):
        print("Please insert dumps into {}".format(source_dir))
        exit(1)

    print("Preparation : start")
    pretty, stemmed = prepare_pretty_stemmed_articles()
    # pretty, stemmed = load_pretty_stemmed_articles()
    print("Prepared articles.")
    #
    dictionary, rev_dict = prepare_dict_rev_dict(stemmed)
    # dictionary,rev_dict = load_dict_reverse_dict()
    print("Dict loaded {} words".format(len(dictionary)))

    # A= load_sparse(regular_matrix_fn)
    A = prepare_regular_matrix(rev_dict, stemmed)
    print("Regular matrix [A] prepared")

    B = prepare_idf_matrix(A)
    print("Idf matrix [B] prepared")

    C = prepare_normalized_idf_matrix(B)
    # C = load_sparse(idf_normalized_matrix_fn)
    print("Normalized Idf matrix [C] prepared")

    for k in k_list[:3]+k_list:
        D = prepare_low_rank_usv(C, k)
        print("Low rank matrix [D], k = {} prepared.".format(k))
