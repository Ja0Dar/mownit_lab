# forms.py

from wtforms import Form, StringField, SelectField

from engine import k_list


class SearchForm(Form):
    choices = [('basic', 'Basic'),  # value,label
               ('basic_norm', 'Basic Normalized'),
               ('idf', 'Inverse document frequency'),
               ('idf_norm', 'IDF normalized')] + [("D{}".format(k), "SVD with k = {}".format(k)) for k in k_list]

    select = SelectField('Search for music:', choices=choices)
    search = StringField('')
