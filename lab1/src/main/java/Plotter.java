import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Plotter {
    private FileWriter fw;
    private BufferedWriter bw;

    public Plotter(String filename) throws IOException {
        fw = new FileWriter(filename);
        bw = new BufferedWriter(fw);


    }


    public void writeLine(String s) throws IOException {
        bw.write(s + "\n");
    }

    public void write(double x, double y) throws IOException {
        bw.write(String.format("%f %f\n", x, y));
    }
    public void write(double x, double y,double z) throws IOException {
        bw.write(String.format("%f %f %f\n", x, y,z));
    }

    public void close() throws IOException {
        bw.close();
        fw.close();
    }

}
