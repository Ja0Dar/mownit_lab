import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Zad3 {
    public static void run() throws IOException {
        List<Float> sValues = Arrays.asList(2f, 3.6667f, 5f, 7.2f, 10f);
        List<Integer> nValues = Arrays.asList(50, 100, 200, 500, 1000);
        float fDzetaSum, fEtaSum;
        double dDzetaSum, dEtaSum, absDzetaErr, absEtaErr, absDzetaErrRev, absEtaErrRev;

        System.out.println("n       s             dzetaErr        dzetaRevErr       etaErr            etaRevErr");
        for (float s : sValues) {
            for (int n : nValues) {
                fDzetaSum = dzetaFloat(s, n, false);
                dDzetaSum = dzetaDouble(s, n, false);
                absDzetaErr = Math.abs(dDzetaSum - fDzetaSum) / dDzetaSum;

                fDzetaSum = dzetaFloat(s, n, true);
                dDzetaSum = dzetaDouble(s, n, true);
                absDzetaErrRev = Math.abs(dDzetaSum - fDzetaSum) / dDzetaSum;

                fEtaSum = etaFloat(s, n, false);
                dEtaSum = etaDouble(s, n, false);
                absEtaErr = Math.abs(dEtaSum - fEtaSum) / dEtaSum;

                fEtaSum = etaFloat(s, n, true);
                dEtaSum = etaDouble(s, n, true);
                absEtaErrRev = Math.abs(dEtaSum - fEtaSum) / dEtaSum;

                System.out.println(String.format("%d     %g      %g      %g      %g      %g", n, s, absDzetaErr, absDzetaErrRev, absEtaErr, absEtaErrRev));


            }
        }
    }


    public static float dzetaFloat(float s, int n, boolean reverseSumDirection) {
        if (n < 1) throw new IllegalArgumentException();
        float sum = 0;

        if (!reverseSumDirection) {
            for (int k = 1; k <= n; k++)
                sum += Math.pow(k, -s);
        } else {

            for (int k = n; k > 0; k--)
                sum += Math.pow(k, -s);
        }
        return sum;
    }

    public static double dzetaDouble(double s, int n, boolean reverseSumDirection) {
        if (n < 1) throw new IllegalArgumentException();
        double sum = 0;

        if (!reverseSumDirection) {
            for (int k = 1; k <= n; k++)
                sum += Math.pow(k, -s);
        } else {

            for (int k = n; k > 0; k--)
                sum += Math.pow(k, -s);
        }
        return sum;
    }

    public static float etaFloat(float s, int n, boolean reverseDirection) {

        if (n < 1) throw new IllegalArgumentException();
        float sum = 0;

        if (!reverseDirection) {
            for (int k = 1; k <= n; k++) {
                if (k % 2 == 0)
                    sum -= Math.pow(k, -s);
                else
                    sum += Math.pow(k, -s);

            }
        } else {
            for (int k = n; k > 0; k--) {
                if (k % 2 == 0)
                    sum -= Math.pow(k, -s);
                else
                    sum += Math.pow(k, -s);

            }
        }

        return sum;
    }

    public static double etaDouble(double s, int n, boolean reverseDirection) {

        if (n < 1) throw new IllegalArgumentException();
        double sum = 0;

        if (!reverseDirection) {
            for (int k = 1; k <= n; k++) {
                if (k % 2 == 0)
                    sum -= Math.pow(k, -s);
                else
                    sum += Math.pow(k, -s);

            }
        } else {
            for (int k = n; k > 0; k--) {
                if (k % 2 == 0)
                    sum -= Math.pow(k, -s);
                else
                    sum += Math.pow(k, -s);

            }
        }

        return sum;
    }


}
