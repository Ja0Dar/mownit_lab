import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Zad1 {

    private static int N = (int)1e8;
    private final static float v = 0.53125f;
    private final static int STEPSIZE = 25000;

    public static void run() {
        float tab[] = new float[N];

        for (int i = 0; i < N; i++) {
            tab[i] = v;
        }


        float sum = 0;

        long startTime = System.nanoTime();
        sum = sumNaive(tab, N);
        long endTime = System.nanoTime();
        presentSumErrorsWithTime(sum, "Naive", endTime - startTime, N, v);

        startTime = System.nanoTime();
        sum = sumRec(tab, 0, N - 1);
        endTime = System.nanoTime();
        presentSumErrorsWithTime(sum, "Recursive", endTime - startTime, N, v);


        try {

            dumpErrPlotData(tab, N);
        } catch (IOException ignored) {
            System.out.println("Couldn't make file data.txt");
        }




        N= 30;
        for (int i = 1; i <= N; i++) {
            tab[i - 1] = (float)Math.pow(4,i);
        }


        sum = sumRec(tab, 0, N - 1);

        System.out.println("Recursive with error - sum of 4^i for i= 1 to N");
        System.out.println(String.format("N = %d ", N));
        double actualSum = Math.pow(4,N+1)-4;//(N/2)* (tab[0]+tab[N-1]);
        double absErr = Math.abs(actualSum - sum);
        double relErr = absErr / actualSum;
        System.out.println(String.format("presice sum : %f, recursive sum : %f", actualSum,sum));
        System.out.println("Abs err:");
        System.out.println(absErr);
        System.out.println("Relative error");
        System.out.println(relErr);

    }


    public static float sumRec(float[] tab, int i, int j) {
//        1.4
        if (j - i > 1) {
            int mid = (i + j) / 2;
            return sumRec(tab, i, mid) + sumRec(tab, mid + 1, j);
        } else if (j - i == 1) {
            return tab[i] + tab[j];
        } else if (j - i == 0) {
            return tab[i];
        } else {
            throw new RuntimeException("sum rec error - sanity check");
        }
    }


    private static float sumNaive(float[] tab, int size) {
        //1.1
        float sum = 0;
        for (int i = 0; i < N; i++) {
            sum = sum + tab[i];
        }
        return sum;
    }

    private static void dumpErrPlotData(float[] tab, int N) throws IOException {
        //1.3
        double absErr, relErr;
        FileWriter fw = new FileWriter("data_no_abs.dat");
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write("# i relErr\n");
        float sum = tab[0];
        for (int i = 1; i < N; i++) {
            sum = sum + tab[i];
            if (i % STEPSIZE == 0) {

                absErr = ((i + 1) * v - sum);
//                absErr = Math.abs((i + 1) * v - sum);

                relErr = absErr / ((i + 1) *v);
                bw.write(String.format("%s %s\n", i, relErr));

            }
        }

        bw.close();
        System.out.println("Dumped data for gnuplot");
    }

    public static void presentSumErrorsWithTime(float sum, String title, long time, int N, float v) {
        //1.2, 1.5
        float absErr;
        float relativeErr;
        absErr = Math.abs(N * v - sum);
        relativeErr = absErr / (N * v);
        System.out.println(String.format("----------%s-------", title));
        System.out.println(String.format("N=%d, v= %f", N, v));
        System.out.println("Sum:");
        System.out.println(sum);

        System.out.println("Abs err:");
        System.out.println(absErr);
        System.out.println("Relative error");
        System.out.println(relativeErr);
        System.out.println("Time [nanos]:");
        System.out.println(time);
        System.out.println("");
    }


}

