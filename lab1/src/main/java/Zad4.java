import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zad4 {
    private static final int XTRESHOLD = 5000;
    private static final int ITERATION_SKIP = 1;

    public static void run() {
        a();
    }


    public static void a() {
        List<Float> x0Values = Arrays.asList(0.13f, 0.3f, 0.5f, 0.7f, 0.9f);
        List<Float> rValues = new ArrayList<>();
        for (float i = 1; i < 4; i += 0.005) {
            rValues.add(i);
        }


        plotMultipleBifurk(x0Values, rValues);
//        plotAllBifurkOnOnePlot(x0Values, rValues);
    }

    private static void plotMultipleBifurk(List<Float> x0Values, List<Float> rValues) {
        Plotter bp;
        for (float x0 : x0Values) {
            try {
                File file = new File(String.format("bifurk_a_%f.dat", x0));
                file.delete();
                bp = new Plotter(String.format("bifurk_a_%f.dat", x0));

                for (float r : rValues) {
                    logisticMapFloatWithPlotting(5 * XTRESHOLD, x0, r, bp);
                }
                bp.close();

            } catch (IOException e) {
            }
        }
    }

    private static void plotAllBifurkOnOnePlot(List<Float> x0Values, List<Float> rValues) {
        Plotter bp;
        try {
            File file = new File("bifurk_a.dat");
            file.delete();
            bp = new Plotter("bifurk_a.dat");

            for (float x0 : x0Values) {
                for (float r : rValues) {
                    logisticMapFloatWithPlotting(5 * XTRESHOLD, x0, r, bp);
                }
            }
            bp.close();

        } catch (IOException e) {
        }

    }

    public static void b() {
        List<Float> x0Values = Arrays.asList(0.13f, 0.3f, 0.5f, 0.7f, 0.9f);
        List<Float> rValues = new ArrayList<>();

        for (float i = 3.75f; i <= 3.8; i += 0.0005) {
            rValues.add(i);
        }

        try {
            File file = new File("bifurk_b_float.dat");
            file.delete();
            Plotter bp = new Plotter("bifurk_b_float.dat");

            for (float x0 : x0Values) {
                for (float r : rValues) {
                    logisticMapFloatWithPlotting(5 * XTRESHOLD, x0, r, bp);
                }
            }
            bp.close();

        } catch (IOException e) {
        }
        try {
            File file = new File("bifurk_b_double.dat");
            file.delete();
            Plotter bp = new Plotter("bifurk_b_double.dat");

            for (float x0 : x0Values) {
                for (float r : rValues) {
                    logisticMapDoubleWithPlotting(5 * XTRESHOLD, x0, r, bp);
                }
            }
            bp.close();

        } catch (IOException e) {
        }

    }


    public static void c() {
        List<Float> x0Values = Arrays.asList(0.13f, 0.33f, 0.5f, 0.87f, 0.67f);
        float r = 4, x;
        int i;
        for (float x0 : x0Values
                ) {
            x = x0;
            i = 0;
            while (x > 0) {
                x = r * x * (1.0f - x);
                i += 1;
                if (i % 10000 == 0)
                    System.out.println(String.format("x0 %f %d th iteration x = %f", x0, i, x));
            }
            System.out.println(String.format("x0= %f, i = %d", x0, i));
        }


    }

    public static float logisticMapFloatWithPlotting(int n, float x0, float r, Plotter bp) {
        if (r <= 0 || x0 < 0 || x0 > 1 || n < 0) {
            throw new IllegalArgumentException(String.format("r= %f, x0=%f, n= %d", r, x0, n));
        }
        try {
            bp.writeLine("#R X");
        } catch (Exception ignored) {
        }

        float x = x0;


        for (int i = 0; i < n; i++) {
            x = r * x * (1 - x);

            if (i > n - XTRESHOLD) {
                if (i % ITERATION_SKIP == 0) {

                    try {
                        bp.write(r, x);
                    } catch (IOException ex) {
                        throw new RuntimeException("Write error");
                    }
                }
            }

        }

        return x;
    }

    public static double logisticMapDoubleWithPlotting(int n, double x0, double r, Plotter bp) {
        if (r <= 0 || x0 < 0 || x0 > 1 || n < 0) {
            throw new IllegalArgumentException(String.format("r= %f, x0=%f, n= %d", r, x0, n));
        }

        try {
            bp.writeLine("#R X");
        } catch (Exception ignored) {
        }
        double x = x0;


        for (int i = 0; i < n; i++) {
            x = r * x * (1 - x);

            if (i > n - XTRESHOLD) {
                if (i % ITERATION_SKIP == 0) {

                    try {
                        bp.write(r, x);
                    } catch (IOException ex) {
                        throw new RuntimeException("Write error");
                    }
                }
            }

        }

        return x;
    }
}
