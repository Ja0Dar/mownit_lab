public class Zad2 {
    private static int N = (int)1e7;
    private static float v = 0.53125f;

    public static void run() {
        float tab[] = new float[N];
        for (int i = 0; i < N; i++) {
            tab[i] = v;
        }

        long startTime = System.nanoTime();
        float sum = kahan(tab,N);
        long endTime = System.nanoTime();
        Zad1.presentSumErrorsWithTime(sum,"Kahan Alg",endTime-startTime,N,v);
    }

    public static float kahan(float tab[], int N) {
        float sum, err;
        sum = 0;
        err = 0;
        for (int i = 0; i < N; i++) {
            float y = tab[i] - err;
            float temp = sum + y;
            err = (temp - sum) - y;
            sum = temp;
        }
        return sum;
    }
}