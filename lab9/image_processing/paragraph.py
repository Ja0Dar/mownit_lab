from collections import Counter
from copy import copy
from typing import List, Tuple

import matplotlib.pyplot as plt
import numpy as np
import skimage.transform as transform
from matplotlib import cm

from image_processing.commons import rotate


def streighten_text_images(img: np.ndarray, original_img: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """

    :type img: np.ndarray
    :type original_img: np.ndarray
    *img* - text image after thresholdingm on black background

    *original_img* - loaded image, on white background
    """

    angles, dists = hough_angles_dists(img)

    skew_angle = np.degrees(dominant_angle(angles))

    if skew_angle<0:
        skew_angle= 180+ skew_angle


    str_img = rotate(img, skew_angle - 90)
    str_original_img = 255 - rotate(255 - original_img,
                                    skew_angle - 90)  # turning to black for rotation to keep white background
    return str_img, str_original_img


def straight_image_to_lines(image: np.ndarray) -> List[np.ndarray]:
    lines = straight_image_to_line_borders(image)
    res = []
    for beg, end in lines:
        res.append(image[beg:end, :])
    return res


def straight_images_to_lines(img: np.ndarray, orig_img: np.ndarray) -> Tuple[List[np.ndarray], List[np.ndarray]]:
    line_borders = straight_image_to_line_borders(img)

    img_lines = []
    orig_img_lines = []
    for beg, end in line_borders:
        img_lines.append(img[beg:end, :])
        orig_img_lines.append(orig_img[beg:end, :])
    return img_lines, orig_img_lines


def straight_image_to_line_borders(str_img: np.ndarray) -> List[Tuple[int, int]]:
    summed_rows = np.sum(str_img, 1)
    lines = []
    i = 0
    beg = 0

    def is_line(s):
        return s > 300

    counting_line = False  # assumption : first row is blank

    # assumption : linegap > 2px

    for y, row_sum in enumerate(summed_rows):
        if counting_line:
            if not is_line(row_sum):
                lines.append((beg, y + 1))  # .loc[i]=[beg,y+1]
                i += 1
                counting_line = False
        else:
            if is_line(row_sum):
                counting_line = True
                beg = y - 1
    return lines


def hough_angles_dists(text_img: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """
    :type text_img: np.ndarray
    *text_img* should be after thresholding.
    :returns angles, dists
    *angles* - list of line angles in radians

    *dists* - list of line "offsets"
    """
    h, theta, d = transform.hough_line(text_img)
    _, angles, dists = transform.hough_line_peaks(h, theta, d)
    return angles, dists


def plot_with_lines(img: np.ndarray, angles: List, dists: List, show=True) -> None:
    fig, axes = plt.subplots(1, 2, figsize=(15, 6),
                             subplot_kw={'adjustable': 'box-forced'})
    ax = axes.ravel()

    ax[0].imshow(img, cmap=cm.gray)
    ax[0].set_title('Input image')
    ax[0].set_axis_off()

    ax[1].imshow(img, cmap=cm.gray)
    ax[1].set_title('Image with lines')
    for angle, dist in zip(angles, dists):
        y0 = (dist - 0 * np.cos(angle)) / np.sin(angle)
        y1 = (dist - img.shape[1] * np.cos(angle)) / np.sin(angle)
        ax[1].plot((0, img.shape[1]), (y0, y1), '-r')

    if show:
        plt.show()


def dominant_angle(angles):
    angles = np.round(copy(angles), 8)
    np.unique(angles)
    c = Counter(angles)
    angle = c.most_common()[0][0]
    return angle
