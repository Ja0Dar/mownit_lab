from typing import Tuple

import numpy as np
import pandas as pd
import skimage.transform as trans
from copy import copy
import matplotlib.pyplot as plt

from image_processing.commons import trim_img_arr


def print_letter_window_shape(font_size):
    return int(8 * font_size / 6), font_size


def test_window_shape(font_size):
    return int(font_size * 1.15), int(font_size * 1.15)


def align_white_letter_on_image(letter, font_size=60):
    shape = test_window_shape(font_size)
    letter = trim_img_arr(letter)

    rows_to_add = shape[0] - letter.shape[0]
    cols_to_add = shape[1] - letter.shape[1]

    res = np.vstack((np.zeros((rows_to_add, letter.shape[1])), letter))
    res = np.hstack((res, np.zeros((res.shape[0], cols_to_add))))
    return res


# performs not ideal (it has to confuse o wih 0)
# but good enough for most chars
def to_test_img_and_ratio(white_letter_img, threshold=128, dest_shape=(28, 28)):
    trimmed_l = copy(trim_img_arr(white_letter_img, 0))
    if threshold is not None:
        trimmed_l[np.where(trimmed_l < threshold)] = 0
        trimmed_l[np.where(trimmed_l >= threshold)] = 255

    return trans.resize(trimmed_l, dest_shape), trimmed_l.shape[0] / trimmed_l.shape[1]


def train_test_split(df: pd.DataFrame, test_percentage: float = 0.8) -> Tuple[pd.DataFrame, pd.DataFrame]:
    msk = np.random.rand(len(df)) < test_percentage
    return df[msk], df[~msk]


def plot_history(history):
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.show()
