from copy import copy

import matplotlib.image as mimg
import matplotlib.pyplot as plt
import numpy as np
import skimage.transform as transform
from matplotlib import cm


def load_grayscale_img(path) -> np.ndarray:
    return np.mean(mimg.imread(path), -1)


def threshold(orig_img: np.ndarray, threshold: int = 150) -> np.ndarray:
    black_background = np.mean(orig_img) < 128

    img = copy(orig_img) if black_background else 255 - orig_img
    img[img < threshold] = 0
    img[img >= threshold] = 255

    return img


def set_big_plot():
    plt.figure(figsize=(18, 16), dpi=80, facecolor='w', edgecolor='k')


def plot_gray(img: np.ndarray, big=False) -> None:
    if big:
        set_big_plot()
    plt.imshow(img, cmap=cm.gray)
    plt.show()


def rotate(image: np.ndarray, angle) -> np.ndarray:
    return transform.rotate(image, angle, True)


def low_rank_approximation(img, precentage=0.25):
    U, s, V_trans = np.linalg.svd(img)
    n = int(s.size * precentage)
    U = U[:, :n]
    V_trans = V_trans[:n, :]
    sigma = np.eye(n) * s[:n]
    return U.dot(sigma.dot(V_trans))


def trim_img_arr(img: np.ndarray, background_color=0, horizontal=True, vertical=True) -> np.ndarray:
    height, width = img.shape
    eps = 1
    def fst_non_0(hist, start, step):
        x = start
        while np.abs(hist[x] - background_color) < eps:
            x += step
        x -= step

        if step > 0:
            return max(start, x)
        else:
            return min(start, x)

    x_start = 0
    x_end = width - 1

    y_start = 0
    y_end = height - 1

    if horizontal:
        x_hist = np.average(img, 0)
        x_start = fst_non_0(x_hist, 0, 1)
        x_end = fst_non_0(x_hist, width - 1, -1) + 1

    if vertical:
        y_hist = np.average(img, 1)
        y_start = fst_non_0(y_hist, 0, 1)
        y_end = fst_non_0(y_hist, height - 1, -1) + 1

    return img[y_start:y_end, x_start:x_end]
