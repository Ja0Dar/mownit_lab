from typing import List, Tuple

import numpy as np

from image_processing.line import lines_to_words, line_to_words
from image_processing.paragraph import straight_images_to_lines, straight_image_to_lines


def straight_images_to_words(img: np.ndarray, orig_img: np.ndarray, font_size=60) -> \
        Tuple[List[List[np.ndarray]], List[List[np.ndarray]]]:
    img_lines, orig_img_lines = straight_images_to_lines(img, orig_img)
    zipped_words = list(map(lambda t: lines_to_words(*t, font_size=font_size),
                            list(zip(img_lines, orig_img_lines))))

    orig_img_words = map(lambda t: t[1], zipped_words)
    img_words = map(lambda t: t[0], zipped_words)

    return list(img_words), list(orig_img_words)


def straight_image_to_words(img: np.ndarray, font_size=60) -> List[List[np.ndarray]]:
    img_lines = straight_image_to_lines(img)
    words = list(map(lambda line: line_to_words(line, font_size=font_size), img_lines))

    return words
