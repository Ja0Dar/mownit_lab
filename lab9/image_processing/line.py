from typing import List, Tuple

import numpy as np
import pandas as pd


def line_to_words(line: np.ndarray, font_size) -> List[np.ndarray]:
    return line_to_words_from_borders(line, line_to_word_borders(line, font_size))


def line_to_words_from_borders(line, word_borders):
    return list(map(lambda t: line[:, t[0]:t[1]], word_borders))


def lines_to_words(line: np.ndarray, orig_line: np.ndarray,font_size) -> Tuple[List[np.ndarray], List[np.ndarray]]:
    word_borders = line_to_word_borders(line,font_size)

    line_words = line_to_words_from_borders(line, word_borders)
    orig_line_words = line_to_words_from_borders(orig_line, word_borders)
    return line_words, orig_line_words


def line_to_word_borders(line, font_size):
    space_borders = spaces_from_gaps(gaps_in_line(line), font_size)
    word_borders = []
    for i in range(1, len(space_borders)):
        word_beg = int(space_borders[i - 1][1])
        word_end = int(space_borders[i][0])
        word_borders.append((word_beg, word_end))
    return word_borders


def spaces_from_gaps(gaps, font_size) -> List[Tuple[int, int]]:
    max_letter_gap = font_size / 5

    gaps_df = pd.DataFrame()
    gaps_df["beg"] = [tup[0] for tup in gaps]
    gaps_df["end"] = [tup[1] for tup in gaps]
    gaps_df["len"] = gaps_df["end"] - gaps_df["beg"]

    n = gaps_df.shape[0]
    default_space_gap = max_letter_gap * 2

    gaps_df["beg"].loc[0] = gaps_df["end"].loc[0] - default_space_gap
    gaps_df["len"].loc[0] = default_space_gap
    gaps_df["end"].loc[n - 1] = gaps_df["beg"].loc[n - 1] + default_space_gap
    gaps_df["len"].loc[n - 1] = default_space_gap

    spaces = gaps_df.where(gaps_df["len"] > max_letter_gap).dropna()
    return list(zip(spaces["beg"], spaces["end"]))


def gaps_in_line(line: np.ndarray) -> List[Tuple[int, int]]:  # not trimmed line
    projected_l = np.average(line, 0)
    projected_l[np.where(projected_l < 15)] = 0
    projected_l[np.where(projected_l >= 15)] = 1

    is_gap_arr = np.abs(1 - projected_l).astype(bool)

    gaps = []
    beg = 0

    counting_gap = True

    for x, is_gap in enumerate(is_gap_arr):
        if counting_gap:
            if not is_gap:
                gaps.append((beg, x))
                counting_gap = False
        else:
            if is_gap:
                counting_gap = True
                beg = x
    gaps.append((beg, projected_l.size))
    return gaps
