from typing import List, Tuple

import numpy as np


def word_to_letters(line: np.ndarray) -> List[np.ndarray]:
    return word_to_letter_from_borders(line, word_to_letter_borders(line))


def word_to_letter_from_borders(line, word_borders):
    return list(map(lambda t: line[:, t[0]:t[1]], word_borders))


def words_to_letters(line: np.ndarray, orig_line: np.ndarray) -> Tuple[List[np.ndarray], List[np.ndarray]]:
    word_borders = word_to_letter_borders(line)

    line_words = word_to_letter_from_borders(line, word_borders)
    orig_line_words = word_to_letter_from_borders(orig_line, word_borders)
    return line_words, orig_line_words


def word_to_letter_borders(line):
    space_borders = gaps_in_word(line)
    word_borders = []
    for i in range(1, len(space_borders)):
        word_beg = int(space_borders[i - 1][1])
        word_end = int(space_borders[i][0])
        word_borders.append((word_beg, word_end))

    word_borders.append((space_borders[-1][1], line.shape[1]))

    return word_borders


def gaps_in_word(line: np.ndarray) -> List[Tuple[int, int]]:  # not trimmed line
    projected_l = np.average(line, 0)
    projected_l[np.where(projected_l < 5)] = 0
    projected_l[np.where(projected_l >= 5)] = 1

    is_gap_arr = np.abs(1 - projected_l).astype(bool)

    gaps = []
    beg = 0

    counting_gap = True

    for x, is_gap in enumerate(is_gap_arr):
        if counting_gap:
            if not is_gap:
                gaps.append((beg, x))
                counting_gap = False
        else:
            if is_gap:
                counting_gap = True
                beg = x
    # gaps.append((beg, projected_l.size))
    return gaps
