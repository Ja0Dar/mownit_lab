from typing import Tuple

from PIL import Image, ImageFont, ImageDraw
import numpy as np
import pandas as pd

from image_processing.classification_helpers import to_test_img_and_ratio
from image_processing.commons import low_rank_approximation


def text_img(text, shape=(80, 60), ttf='fonts/arial.ttf', white_text=True, font_size=60) -> np.ndarray:

    background = 0 if white_text else 255
    font_color = 255 if white_text else 0
    # Pil operates on "x,y" not "row,col"
    image = Image.new('L', (shape[1], shape[0]), background)  # blank image in grayscale
    font = ImageFont.truetype(ttf, font_size)
    drawing_context = ImageDraw.Draw(image)
    drawing_context.text((0, 0), text, font=font, fill=font_color)

    return np.asarray(image)


def low_rank_approx_text_img(text, shape, ttf, low_rang_percentage=0.4, white_text=True, font_size=60):
    img = text_img(text, shape, ttf, white_text, font_size)

    res = low_rank_approximation(np.asarray(img), low_rang_percentage)
    res[np.where(res < 40)] = 0
    return


def generate_letter_data(font_paths, chars, font_size, img_shape: Tuple[int, int] = (28, 28),
                         with_ratio=True, noise_reduction: float = None) -> pd.DataFrame:
    data = pd.DataFrame(columns=["char", "index", "img", "ratio"])

    i = 0
    for font in font_paths:
        for ind, letter in enumerate(chars):
            # img = to_test_img_align( #todo - doesnt work well - remove?
            #     text_img(letter, print_letter_window_shape(test_font), ttf=font, font_size=font_size),
            #     font_size)
            img_ = text_img(letter, font_size=font_size, ttf=font)
            img, ratio = to_test_img_and_ratio(
                img_ if noise_reduction is None else low_rank_approximation(img_, noise_reduction)
                , dest_shape=img_shape
            )

            data.loc[i] = [letter, ind, img/np.max(img), ratio]
            i += 1
    data["features"] = list(map(lambda t: np.hstack(t),
                                zip(data.img.map(lambda img: img.ravel()), data.ratio))) \
        if with_ratio else data.img.map(lambda image: image.ravel())

    return data
