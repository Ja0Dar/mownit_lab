import numpy as np
import pandas as pd
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense
from image_processing.classification_helpers import train_test_split, to_test_img_and_ratio
from image_processing.commons import low_rank_approximation
from image_processing.word import word_to_letters


def get_model(input_dim, classes: int):
    model = Sequential()
    model.add(Dense(classes, input_dim=input_dim, activation='softmax'))
    return model


def train_ocr_model(data: pd.DataFrame, batch_size=64, epochs=50):
    train_data, test_data = train_test_split(data, 0.85)

    classes = data["index"].unique().size
    input_dim = data["features"].loc[0].size

    x = np.matrix(train_data["features"].tolist(), dtype=float)
    y = np.matrix(train_data["index"].tolist(), dtype=float)

    x_val = np.matrix(test_data["features"].tolist(), dtype=float)
    y_val = np.matrix(test_data["index"].tolist(), dtype=float)

    Y = np_utils.to_categorical(y, classes)
    Y_val = np_utils.to_categorical(y_val, classes)

    model = get_model(input_dim, classes)

    model.compile(optimizer='sgd', loss='categorical_crossentropy', metrics=['accuracy'])
    history = model.fit(x, Y, batch_size=batch_size, epochs=epochs, validation_data=(x_val, Y_val))
    score = model.evaluate(x_val, Y_val, verbose=0)
    print('Test score:', score[0])
    print('Test accuracy:', score[1])

    return model, history


def letter_img_to_str(img, model, accepted_chars, with_ratio, noise_reduction: float, img_shape):
    if noise_reduction is not None:
        img = low_rank_approximation(img, noise_reduction)

    img, ratio = to_test_img_and_ratio(img, dest_shape=img_shape)
    features = np.hstack((img.ravel(), [ratio])) if with_ratio else img.ravel()

    prediction = model.predict(np.matrix(features))
    return accepted_chars[np.argmax(prediction)]


def ocr(img_words, model, accepted_chars, with_ratio=True, noise_reduction: float = None):
    # assumption : square shape
    features = int(model.input.shape[1])
    x = int(np.sqrt(features - 1)) if with_ratio else int(np.sqrt(features))
    img_shape = (x, x)

    return "\n".join(map(lambda line:
                         " ".join(
                             map(lambda word:
                                 "".join(
                                     map(lambda letter: letter_img_to_str(letter, model, accepted_chars, with_ratio,
                                                                          noise_reduction, img_shape)
                                         ,
                                         word_to_letters(word))
                                 ), line)
                         )
                         , img_words))
