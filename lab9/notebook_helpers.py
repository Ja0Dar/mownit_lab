from collections import Counter

import plotly.graph_objs as go
import plotly.offline as py

py.init_notebook_mode(connected=True)


def plotly_sums(sums, name="Generic", title="Generic", xlabel="Generic", ylabel="Generic"):
    trace1 = go.Scatter(
        x=[i for i in range(1, len(sums))],
        y=sums,
        name=name,
        mode='lines+markers',
    )

    data = [trace1]
    layout = go.Layout(
        barmode='stack',
        title=title,
        width=1000,
        height=600,
        paper_bgcolor='rgb(243, 243, 243)',
        plot_bgcolor='rgb(243, 243, 243)',
        yaxis=dict(title=xlabel),
        xaxis=dict(title=ylabel)
    )

    fig = go.Figure(data=data, layout=layout)
    py.iplot(fig)


def count_letters(text):
    c = Counter()
    for letter in text:
        c[letter] += 1
    return c
